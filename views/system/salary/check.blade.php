@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row mb-3">
                <div class="col-12 mt-2">
                    <h3 class="card-title">{{ __('Salary Auto-Generator') }}</h3>
                </div>
                <div class="col-12 mt-2">
                    <div class="alert alert-info">
                        {{ __('Current Period') }}:
                        <span class="badge badge-info">{{ $currentPeriod->periodText }}</span>
                    </div>
                </div>
            </div>
            {!! Form::open(['route' => 'salary.generate']) !!}
                <div class="row">
                    @forelse ($employees as $employee)
                        <div class="col-4 mb-3">
                            @php ($added = $employee->hasSalaryInPeriod($currentPeriod))
                            {!! Form::checkboxField('employees[' . $employee->id . ']',
                                    $employee->full_name . ' ' . ($added ? '*' : null),
                                    ['checked' => !$added, 'disabled' => $added, 'value' => $employee->id]
                            ) !!}
                        </div>
                    @empty
                        @include('system.partials.noRecord')
                    @endforelse

                    <div class="col-12 text-right">
                        <a href="{{ route('salary.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        {!! Form::submit(__('Generate'), ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
            <br />
        </div>
    </div>
@endsection