{!! Form::errorList($errors) !!}

<div class="form-group row">
    <div class="col-xl-6">
        {!! Form::selectFieldTh('employee_id', $employees, __('Employee')) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::dateFieldTh('date_at', __('Date')) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::textFieldTh('salary') !!}
    </div>
    <div class="col-xl-6">
        {!! Form::selectFieldTh('currency', $model::currencies()) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::selectFieldTh('rate_type', $model::rateTypes()) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::selectFieldTh('gateway', $model::gateways()) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::textFieldTh('overtime', __('Overtime (hours)')) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::textFieldTh('bonus') !!}
    </div>
    <div class="col-xl-6">
        {!! Form::selectFieldTh('entrepreneur', $model::entrepreneurStatuses()) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::selectFieldTh('district_id', $districts, __('District')) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::selectFieldTh ('status', $model::booleanStatuses(), __('Paid')) !!}
    </div>
</div>

{!! Form::buttons(route('salary.index')) !!}