{!! Form::openFilter(['route' => ['salary.index'], 'method' => 'get']) !!}
    <div class="row mt-2 mb-3">
        {!! Form::selectFieldFl('period_id', $periods, __('Period')) !!}
        {!! Form::selectFieldFl('employee_id', $employees, __('Employee')) !!}
        {!! Form::selectFieldFl('currency', $filters::currencies()) !!}
        {!! Form::selectFieldFl('rate_type', $filters::rateTypes()) !!}
        {!! Form::selectFieldFl('entrepreneur', $filters::entrepreneurStatuses()) !!}
        {!! Form::selectFieldFl('gateway', $filters::gateways()) !!}
        {!! Form::selectFieldFl('status', $filters::booleanStatuses(), __('Paid')) !!}
        {!! Form::filterButtons(route('salary.index')) !!}
    </div>
{!! Form::close() !!}