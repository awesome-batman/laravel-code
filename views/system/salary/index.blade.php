@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row mb-3">
                <div class="col-6 mt-2">
                    <h3 class="card-title">{{ __('All Salaries') }}</h3>
                </div>
                <div class="col-6 text-right">
                    <a href="{{ route('salary.check') }}" class="btn btn-warning btn-rounded">
                        {{ __('Auto-Generate') }}&nbsp;
                        <i class="fas fa-car-side ml-1"></i>
                    </a>
                    <a href="{{ route('salary.create') }}" class="btn btn-success btn-rounded">
                        <i class="mdi mdi-plus"></i>
                        {{ __('Add Salary') }}
                    </a>
                </div>
            </div>
            @include('system.salary.partials.filters')
            <div class="table-wrapper">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th>{{ __('#') }}</th>
                        <th>
                            {{ __('Date') }}, {{ __('Period') }}
                        </th>
                        <th>{{ __('Employee') }}</th>
                        <th>{{ __('Salary') }}</th>
                        <th>{{ __('Note') }}</th>
                        <th>{{ __('Overtime (hours)') }}</th>
                        <th>{{ __('Bonus') }}</th>
                        <th>{{ __('Entrepreneur') }}</th>
                        <th>{{ __('Payment Accounts') }}</th>
                        <th>{{ __('Paid') }}</th>
                        <th>{{ __('Salary Payment') }}</th>
                        <th>{{ __('Total') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    @forelse ($models as $model)
                        <tr @if ($model->period_id != $currentPeriod->id) class="text-muted" @endif>
                            <td>{{ $loop->iteration }}</td>
                            <td>
                                {{ $model->date }}
                                @if (!$model->isCorrectPeriod())
                                    <span class="badge badge-danger">{{ __('Incorrect') }}</span>
                                @endif
                            </td>
                            <td>
                                {{ $model->employeeName }}
                            </td>
                            <td>
                                <strong>{{ $model->salary }}</strong>&nbsp;
                                {!! Form::currencyBadge($model->currencyText) !!}
                                <span class="badge badge-light">{{ $model->rateTypeCaption }}</span>
                            </td>
                            <td>
                                @if ($model->period_id == $currentPeriod->id)
                                    {!! nl2br(optional($model->employee)->note) !!}
                                @endif
                            </td>
                            <td>
                                @if (!empty($model->overtime))
                                    {{ $model->overtime }} {{ __('hours') }}
                                    ({{ round($model->overtimePayment, 2) }})
                                @endif
                            </td>
                            <td>
                                @if (!empty($model->bonus))
                                    {{ $model->bonus }}
                                    {{ $model->currencyText }}
                                @endif
                            </td>
                            <td>
                                <span class="badge badge-{{ $model->entrepreneurBadgeClass }}">{{ $model->entrepreneurText }}</span>
                                @if (!empty($model->entrepreneur != $model::ENTREPRENEUR_NO))
                                    <br />
                                    ({{ __('Pension') }}: {{ $model->periodPensionTax }} UAH,
                                    {{ __('Unified') }}: {{ round($model->unifiedTaxUahPayment, 2) }} UAH)
                                @endif
                            </td>
                            <td>
                                @if (!empty($model->gateway))
                                    <span class="badge badge-{{ $model->gatewayBadgeClass }}">{{ $model->gatewayText }}</span>
                                @endif
                                @if (!empty($model->payonerPayment))
                                    ({{ round($model->payonerPayment, 2) }} {{ $model->currencyText }})
                                @endif
                                @php ($paymentAccounts = optional($model->employee)->payment_accounts)
                                @if (!empty($paymentAccounts))
                                    <br />
                                    {!! nl2br($paymentAccounts) !!}
                                @endif
                            </td>
                            <td>
                                <span class="badge badge-{{ $model->statusBadgeClass }}">{{ $model->statusText }}</span>
                            </td>
                            <td>
                                {{ round($model->salaryPayment, 2) }}
                                {{ $model->currencyText }}
                            </td>
                            <td>
                                <strong>
                                    {{ round($model->total, 2) }} {{ $model->currencyText }}
                                </strong>
                                <br />
                                ({{ round($model->oppositeTotal, 2) }} {{ $model->oppositeCurrencyText }})
                            </td>
                            <td class="text-nowrap">
                                <a href="{{ route('salary.edit', ['salary' => $model->id]) }}" title="{{ __('Edit') }}" class="btn btn-primary btn-sm fa fa-pencil"></a>
                                <a href="{{ route('salary.file', ['salary' => $model->id]) }}" title="{{ __('Invoice') }}" class="btn btn-success btn-sm fa fa-file-pdf"></a>
                                @if (!$model->status)
                                    <a href="{{ route('salary.setPaid', ['salary' => $model->id]) }}" title="{{ __('Pay') }}" class="btn btn-warning btn-sm fa fa-money"></a>
                                @endif
                                <a href="{{ route('salary.destroy', ['salary' => $model->id]) }}" title="{{ __('Delete') }}" class="btn btn-danger btn-sm fa fa-trash" @click="confirmDelete($event)"></a>
                            </td>
                        </tr>
                    @empty
                        @include('system.partials.noRecord')
                    @endforelse
                </table>
                {{ $models->links() }}
            </div>
            @include('system.employee.partials.statistics')
        </div>
    </div>
@endsection