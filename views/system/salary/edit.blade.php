@extends('layouts.backend')

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    {!! Form::model($model, ['route' => ['salary.update', $model->id]]) !!}
                        @method('PUT')

                        @php ($note = optional($model->employee)->note)
                        @if (!empty($note))
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">{{ __('Note') }}</label>
                                <div class="col-md-10">
                                    <div class="alert alert-warning mb-1">{!! nl2br($note) !!}</div>
                                </div>
                            </div>
                        @endif
                        @include('system.salary.partials.fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title pb-3">{{ __('Total Amount') }}</h4>
                    <p class="form-group alert alert-primary">
                        {{ round($model->total, 2) }}
                        {{ $model->currencyText }}
                    </p>

                    <h4 class="card-title pb-3">{{ __('Payment Accounts') }}</h4>
                    <p class="form-group alert alert-primary">
                        @php ($paymentAccounts = $model->employee->payment_accounts)
                        @if (!empty($paymentAccounts))
                            {!! nl2br($paymentAccounts) !!}
                        @else
                            <span class="text-muted">{{ __('Not Set') }}</span>
                        @endif
                    </p>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title pb-3">{{ __('Statistics') }}</h4>
                    @if (isset($model->created_by))
                        <p class="form-group">
                            {{ __('Created By') }}:
                            <span class="text-muted">
                        @if ($model->created_by)
                            {{ $model->createdByText }},
                            &lt;{{ $model->createdBy->email }}&gt;
                        @else
                            -
                        @endif
                    </span>
                        </p>
                    @endif
                    <p class="form-group">
                        {{ __('Created At') }}:
                        <span class="text-muted">
                            @if ($model->created_at)
                                {{ $model->createdDate }} {{ $model->createdTime }}
                            @else
                                -
                            @endif
                        </span>
                    </p>
                    <p class="form-group">
                        {{ __('Updated At') }}:
                        <span class="text-muted">
                            @if ($model->updated_at)
                                {{ $model->updatedDate }} {{ $model->updatedTime }}
                            @else
                                -
                            @endif
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
