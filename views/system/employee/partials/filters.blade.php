{!! Form::openFilter(['route' => ['employee.index'], 'method' => 'get']) !!}
    <div class="row mt-2 mb-3">
        {!! Form::textFieldFl('full_name') !!}
        {!! Form::selectFieldFl('active', $filters::booleanStatuses()) !!}
        {!! Form::selectFieldFl('rate_type', $filters::rateTypes()) !!}
        {!! Form::selectFieldFl('entrepreneur', $filters::entrepreneurStatuses()) !!}
        {!! Form::selectFieldFl('gateway', $filters::gateways(), __('Default Gateway')) !!}
        {!! Form::filterButtons(route('employee.index')) !!}
    </div>
{!! Form::close() !!}