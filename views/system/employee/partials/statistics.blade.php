<div class="row mb-3 mt-2">
    <div class="col-4">
        <h3 class="card-title">{{ __('Statistics') }}</h3>
        @if (!empty($currentPeriod))
            {{ __('Current period') }}: <strong>{{ $currentPeriod->periodText }}</strong><br />
            {{ __('Working days in month') }}: <strong>{{ $currentPeriod->working_days }}</strong><br />
            {{ __('Pension tax') }}: <strong>{{ $currentPeriod->pension_tax }}</strong><br />
            {{ __('Dollar rate') }}: <strong>{{ $currentPeriod->dollar_rate }}</strong><br />
        @else
            <span class="text-danger">{{ __('Current period is not set') }}.</span>
        @endif
    </div>
    <div class="col-4">
        <h3 class="card-title">{{ __('Salaries: Current period') }}</h3>
        {{ __('Total salary costs in USD') }}: <strong>{{ round($currentPeriod->getTotalSalaryAmount(), 2) }}</strong> USD<br />
        {{ __('Total salary costs in UAH') }}: <strong>{{ round($currentPeriod->getUahTotalSalaryAmount(), 2) }}</strong> UAH<br />
        <br />
        {{ __('Payoneer amount') }}: <strong>{{ round($currentPeriod->getPayoneerSalaryAmount(), 2) }}</strong> USD<br />
        {{ __('Bank amount') }}: <strong>{{ round($currentPeriod->getBankSalaryAmount(), 2) }}</strong> USD<br />
        {{ __('Cache amount') }}: <strong>{{ round($currentPeriod->getCacheSalaryAmount(), 2) }}</strong> USD<br />
        <br />
        {{ __('Paid amount') }}: <strong>{{ round($currentPeriod->getPaidSalaryAmount(), 2) }}</strong> USD<br />
        {{ __('Debt amount') }}: <strong>{{ round($currentPeriod->getDebtSalaryAmount(), 2) }}</strong> USD<br />
    </div>
    <div class="col-4">
        <h3 class="card-title">{{ __('Salaries: Estimated') }}</h3>
        {{ __('Total salary costs in USD') }}: <strong>{{ round($service->getTotalSalaryAmount(), 2) }}</strong> USD<br />
        {{ __('Total salary costs in UAH') }}: <strong>{{ round($service->getUahTotalSalaryAmount(), 2) }}</strong> UAH<br />
        <br />
        {{ __('Payoneer amount') }}: <strong>{{ round($service->getPayoneerSalaryAmount(), 2) }}</strong> USD<br />
        {{ __('Bank amount') }}: <strong>{{ round($service->getBankSalaryAmount(), 2) }}</strong> USD<br />
        {{ __('Cache amount') }}: <strong>{{ round($service->getCacheSalaryAmount(), 2) }}</strong> USD<br />
    </div>
</div>