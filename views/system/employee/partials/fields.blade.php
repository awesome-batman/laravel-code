{!! Form::errorList($errors) !!}

<div class="form-group row">
    <div class="col-xl-6">
        {!! Form::textFieldTh('full_name') !!}
    </div>
    <div class="col-xl-6">
        {!! Form::textFieldTh('salary') !!}
    </div>
    <div class="col-xl-6">
        {!! Form::textFieldTh('salary_trial') !!}
    </div>
    <div class="col-xl-6">
        {!! Form::selectFieldTh('currency', $model::currencies()) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::selectFieldTh('rate_type', $model::rateTypes()) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::textAreaFieldTh('note') !!}
    </div>
    <div class="col-xl-6">
        {!! Form::textAreaFieldTh('payment_accounts', null) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::selectFieldTh('gateway', $model::gateways(), __('Default Gateway')) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::selectFieldTh('entrepreneur', $model::entrepreneurStatuses()) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::selectFieldTh('district_id', $districts, __('District')) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::dateFieldTh('started_at', __('Started Date')) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::dateFieldTh('trial_end_at', __('Trial End Date')) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::textFieldTh('overtime', __('Overtime (hours)')) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::textFieldTh('bonus') !!}
    </div>
    <div class="col-xl-6">
        {!! Form::selectFieldTh('active', $model::booleanStatuses()) !!}
    </div>
    <div class="col-xl-6">
        @if (!empty($model->id))
            {!! Form::textFieldTh('total', null, ['disabled', 'value' => round($model->total, 2)]) !!}
        @endif
    </div>
    <div class="col-xl-12">
        <hr />
        <h5 class="card-title">{{ __('Invoice Info') }}</h5>
    </div>
    <div class="col-xl-6">
        {!! Form::textFieldTh('id_code', __('Identification Code')) !!}
    </div>
    <div class="col-xl-6">
        {!! Form::textFieldTh('phone') !!}
    </div>
    <div class="col-xl-6">
        {!! Form::textFieldTh('address') !!}
    </div>
    <div class="col-xl-6">
        {!! Form::textAreaFieldTh('payment_details') !!}
    </div>
</div>

{!! Form::buttons(route('employee.index')) !!}