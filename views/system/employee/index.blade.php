@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row mb-3">
                <div class="col-6 mt-2">
                    <h3 class="card-title">{{ __('All Employees') }}</h3>
                </div>
                <div class="col-6 text-right">
                    <a href="{{ route('employee.create') }}" class="btn btn-success btn-rounded">
                        <i class="mdi mdi-plus"></i>
                        {{ __('Add Employee') }}
                    </a>
                </div>
            </div>
            @include('system.employee.partials.filters')
            <div class="table-wrapper">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th>{{ __('#') }}</th>
                        <th>{{ __('Full Name') }}</th>
                        <th>{{ __('Salary') }}</th>
                        <th>{{ __('Note') }}</th>
                        <th>{{ __('Overtime (hours)') }}</th>
                        <th>{{ __('Bonus') }}</th>
                        <th>{{ __('Entrepreneur') }}</th>
                        <th>{{ __('Payment Accounts') }}</th>
                        <th>{{ __('Default Gateway') }}</th>
                        <th>{{ __('Salary Payment') }}</th>
                        <th>{{ __('Total') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    @forelse ($models as $model)
                        <tr @if (!$model->active) class="text-muted" @endif>
                            <td>{{ $loop->iteration }}</td>
                            <td>
                                {{ $model->full_name }}
                                @if (!$model->active)
                                    <span class="badge badge-danger">{{ __('Not Active') }}</span>
                                @endif
                            </td>
                            <td>
                                <strong>{{ $model->salary }}</strong>&nbsp;
                                {!! Form::currencyBadge($model->currencyText) !!}
                                @if (!empty($model->salary_trial))
                                    ({{ __('Trial') }}: {{ $model->salary_trial }} {{ $model->currencyText }})
                                @endif
                                <span class="badge badge-light">{{ $model->rateTypeCaption }}</span>
                            </td>
                            <td>
                                {!! nl2br($model->note) !!}
                            </td>
                            <td>
                                @if (!empty($model->overtime))
                                    {{ $model->overtime }} {{ __('hours') }}
                                    ({{ round($model->overtimePayment, 2) }})
                                @endif
                            </td>
                            <td>
                                @if (!empty($model->bonus))
                                    {{ $model->bonus }}
                                    {{ $model->currencyText }}
                                @endif
                            </td>
                            <td>
                                <span class="badge badge-{{ $model->entrepreneurBadgeClass }}">{{ $model->entrepreneurText }}</span>
                                @if (!empty($model->entrepreneur != $model::ENTREPRENEUR_NO))
                                    <br />
                                    ({{ __('Pension') }}: {{ $model->periodPensionTax }} UAH,
                                    {{ __('Unified') }}: {{ round($model->unifiedTaxUahPayment, 2) }} UAH)
                                @endif
                            </td>
                            <td>{!! nl2br($model->payment_accounts) !!}</td>
                            <td>
                                @if (!empty($model->gateway))
                                    <span class="badge badge-{{ $model->gatewayBadgeClass }}">{{ $model->gatewayText }}</span>
                                @endif
                                @if (!empty($model->payonerPayment))
                                    <br />
                                    ({{ round($model->payonerPayment, 2) }} {{ $model->currencyText }})
                                @endif
                            </td>
                            <td>
                                {{ round($model->salaryPayment, 2) }}
                                {{ $model->currencyText }}
                            </td>
                            <td>
                                <strong>
                                    {{ round($model->total, 2) }} {{ $model->currencyText }}
                                </strong>
                                <br />
                                ({{ round($model->oppositeTotal, 2) }} {{ $model->oppositeCurrencyText }})
                            </td>
                            <td class="text-nowrap">
                                <a href="{{ route('employee.edit', ['employee' => $model->id]) }}" title="{{ __('Edit') }}" class="btn btn-primary btn-sm fa fa-pencil"></a>
                                <a href="{{ route('employee.destroy', ['employee' => $model->id]) }}" title="{{ __('Delete') }}" class="btn btn-danger btn-sm fa fa-trash" @click="confirmDelete($event)"></a>
                            </td>
                        </tr>
                    @empty
                        @include('system.partials.noRecord')
                    @endforelse
                </table>
                {{ $models->links() }}
            </div>
            @include('system.employee.partials.statistics')
        </div>
    </div>
@endsection