@extends('layouts.backend')

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    {!! Form::model($model, ['route' => ['employee.update', $model->id]]) !!}
                        @method('PUT')
                        @include('system.employee.partials.fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @include('system.partials.statistics')
    </div>
@endsection
