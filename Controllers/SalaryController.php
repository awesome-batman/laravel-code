<?php

namespace App\Http\Controllers\System;

use PDF;
use App\Models\Salary as Model;
use App\Models\Period;
use App\Models\Employee;
use App\Models\District;
use App\Http\Requests\SalaryRequest as Request;
use App\Services\SalaryService as Service;

class SalaryController extends SystemController
{
    /**
     * @var string
     */
    public $controllerName = 'salary';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $models = Model::filter()
            ->defaultOrder()
            ->paginate($this->itemsPerPage);

        return $this->view('index', [
            'models' => $models,
            'filters' => new Model(),
            'service' => new Service(),
            'currentPeriod' => Period::current(),
            'periods' => Period::list(),
            'employees' => Employee::list(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return $this->view('create', [
            'model' => new Model(),
            'employees' => Employee::list(),
            'districts' => District::list(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        (new Model)->fill($request->all())->save();

        return $this->redirect('index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Model  $salary
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Model $salary)
    {
        return $this->view('edit', [
            'model' => $salary,
            'employees' => Employee::list(),
            'districts' => District::list(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Model  $salary
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(Request $request, Model $salary)
    {
        $salary->fill($request->all())->save();

        return $this->redirect('index');
    }

    /**
     * @param Model $salary
     * @return mixed
     */
    public function file(Model $salary)
    {
        $content = $this->view('file', [
            'model' => $salary,
        ]);

        $pdf = PDF::loadHtml($content);

        return $pdf->download('Invoice-' . $salary->fullId . '.pdf');
    }

    /**
     * @param Model $salary
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function setPaid(Model $salary)
    {
        $salary->status = true;
        $status = $salary->save();

        return $this->redirect('index')
            ->with($status ? 'status' : null, __('The record is marked as paid.'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function check()
    {
        $employees = Employee::active()->get();

        return $this->view('check', [
            'employees' => $employees,
            'currentPeriod' => Period::current(),
        ]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function generate()
    {
        $employeeIds = request('employees');
        $currentPeriod = Period::current();

        $service = new Service();
        $number = $service->generateRecords($employeeIds, $currentPeriod);

        return $this->redirect('index')
            ->with($number ? 'status' : 'error', __($number . ' new salary records were added.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Model  $salary
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Model $salary)
    {
        $status = $salary->delete();

        return $this->redirect('index')
            ->with($status ? 'status' : null, __('The record is deleted.'));
    }
}
