<?php

namespace App\Http\Controllers\System;

use App\Models\Employee as Model;
use App\Models\Period;
use App\Models\District;
use App\Http\Requests\EmployeeRequest as Request;
use App\Services\SalaryService as Service;

class EmployeeController extends SystemController
{
    /**
     * @var string
     */
    public $controllerName = 'employee';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $models = Model::filter()
            ->defaultOrder()
            ->paginate($this->itemsPerPage);

        return $this->view('index', [
            'models' => $models,
            'filters' => new Model(),
            'service' => new Service(),
            'currentPeriod' => Period::current(),
            'districts' => District::list(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return $this->view('create', [
            'model' => new Model(),
            'districts' => District::list(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        (new Model)->fill($request->all())->save();

        return $this->redirect('index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Model  $employee
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Model $employee)
    {
        return $this->view('edit', [
            'model' => $employee,
            'districts' => District::list(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Model  $employee
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(Request $request, Model $employee)
    {
        $employee->fill($request->all())->save();

        return $this->redirect('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Model  $employee
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Model $employee)
    {
        $status = $employee->delete();

        return $this->redirect('index')
            ->with($status ? 'status' : null, __('The record is deleted.'));
    }
}
