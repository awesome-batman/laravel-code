<?php

namespace App\Models;

use Carbon\Carbon;
use App\Traits\CreatedByTrait;
use App\Traits\Finance\CurrencyTrait;
use App\Traits\Finance\PeriodTrait;
use App\Traits\Finance\SalaryTrait;

class Salary extends BaseModel
{
    use CreatedByTrait;
    use CurrencyTrait;
    use PeriodTrait;
    use SalaryTrait;

    const CURRENCY_UAH = 1;
    const CURRENCY_USD = 2;
    const CURRENCY_EUR = 3;

    const RATE_TYPE_MONTHLY = 1;
    const RATE_TYPE_HOURLY = 2;

    const GATEWAY_PAYONEER = 1;
    const GATEWAY_BANK = 2;
    const GATEWAY_CACHE = 3;

    const ENTREPRENEUR_NO = 0;
    const ENTREPRENEUR_PENSION_COVERAGE = 1;
    const ENTREPRENEUR_FULL_COVERAGE = 2;

    /**
     * @var string
     */
    public static $title = 'employeeName';

    /**
     * @var Setting
     */
    private $setting;

    /**
     * @var array
     */
    protected $fillable = [
        'date_at',
        'employee_id',
        'salary',
        'currency',
        'rate_type',
        'overtime',
        'bonus',
        'entrepreneur',
        'gateway',
        'district_id',
        'status',
        'from_date_at',
        'to_date_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'period_id' => 'integer',
        'date_at' => 'date',
        'employee_id' => 'integer',
        'salary' => 'integer',
        'currency' => 'integer',
        'rate_type' => 'integer',
        'entrepreneur' => 'integer',
        'gateway' => 'integer',
        'district_id' => 'integer',
        'status' => 'boolean',
        'from_date_at' => 'from_date',
        'to_date_at' => 'to_date',
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'rate_type' => self::RATE_TYPE_MONTHLY,
        'status' => false,
    ];

    /**
     * @var array
     */
    protected $order = [
        'period_id' => 'desc',
        'created_at' => 'asc',
        'id' => 'asc',
    ];

    /**
     * @var array
     */
    protected $with = [
        'period',
        'employee',
    ];

    /**
     * Salary constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->setting = new Setting();

        static::saving(function ($model) {
            self::associatePeriod($model);
        });

        parent::__construct($attributes);

        $this->initDateAt();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this->belongsTo(District::class);
    }

    /**
     * @return string
     */
    public function getFullIdAttribute()
    {
        return optional($this->employee)->abbreviatedName . '-' . $this->monthFromStartOfEntrepreneurship;
    }

    /**
     * @return int
     */
    public function getMonthFromStartAttribute()
    {
        $entrepreneurDate = optional($this->employee)->started_at;
        $entrepreneurMonth = Carbon::parse($entrepreneurDate)->floorMonth();
        $invoiceMonth = Carbon::parse($this->date_at)->floorMonth();
        $difference = $entrepreneurMonth->diffInMonths($invoiceMonth);

        return $difference;
    }

    /**
     * @return mixed
     */
    public function getWorkingHoursAttribute()
    {
        return optional($this->period)->workingHours;
    }

    /**
     * @return string
     */
    public function getDueDateAttribute()
    {
        $daysAddedForDueDate = $this->setting->daysAddedForDueDate;

        return !empty($this->date_at)
            ? Carbon::parse($this->date_at)->addDays($daysAddedForDueDate)->toFormattedDateString()
            : null;
    }

    /**
     * @return null
     */
    public function getEmployeeNameAttribute()
    {
        return optional($this->employee)->full_name;
    }

    /**
     * @return string
     */
    public function getDateAttribute()
    {
        return !empty($this->date_at)
            ? Carbon::parse($this->date_at)->toFormattedDateString()
            : null;
    }
}