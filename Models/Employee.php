<?php

namespace App\Models;

use Carbon\Carbon;
use App\Traits\CreatedByTrait;
use App\Traits\Finance\CurrencyTrait;
use App\Traits\Finance\PeriodTrait;
use App\Traits\Finance\SalaryTrait;

class Employee extends BaseModel
{
    use CreatedByTrait;
    use CurrencyTrait;
    use PeriodTrait;
    use SalaryTrait;

    const CURRENCY_UAH = 1;
    const CURRENCY_USD = 2;
    const CURRENCY_EUR = 3;

    const RATE_TYPE_MONTHLY = 1;
    const RATE_TYPE_HOURLY = 2;

    const GATEWAY_PAYONEER = 1;
    const GATEWAY_BANK = 2;
    const GATEWAY_CACHE = 3;

    const ENTREPRENEUR_NO = 0;
    const ENTREPRENEUR_PENSION_COVERAGE = 1;
    const ENTREPRENEUR_FULL_COVERAGE = 2;

    /**
     * @var string
     */
    public static $title = 'full_name';

    /**
     * @var mixed|null
     */
    public $period = null;

    /**
     * @var Setting
     */
    private $setting;

    /**
     * @var array
     */
    protected $fillable = [
        'full_name',
        'salary',
        'salary_trial',
        'currency',
        'rate_type',
        'note',
        'overtime',
        'bonus',
        'entrepreneur',
        'payment_accounts',
        'gateway',
        'district_id',
        'active',
        'started_at',
        'trial_end_at',
        'id_code',
        'address',
        'phone',
        'payment_details',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'salary' => 'integer',
        'currency' => 'integer',
        'rate_type' => 'integer',
        'entrepreneur' => 'integer',
        'gateway' => 'integer',
        'district_id' => 'integer',
        'active' => 'boolean',
        'started_at' => 'date',
        'trial_end_at' => 'date',
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'rate_type' => self::RATE_TYPE_MONTHLY,
        'active' => true,
    ];

    /**
     * @var array
     */
    protected $search = [
        'id',
        'full_name',
        'payment_accounts',
        'id_code',
        'address',
        'phone',
        'payment_details',
    ];

    /**
     * @var string[]
     */
    protected $only = [
        'full_name',
        'id',
    ];

    /**
     * Employee constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->period = Period::current();
        $this->setting = new Setting();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this->belongsTo(District::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function salaries()
    {
        return $this->hasMany(Salary::class);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    /**
     * @return string
     */
    public function getAbbreviatedNameAttribute()
    {
        $words = explode(' ', $this->full_name);
        $value = '';

        foreach ($words as $word) {
            $value .= mb_substr($word, 0, 1);
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getStartedDateAttribute()
    {
        return !empty($this->started_at)
            ? Carbon::parse($this->started_at)->toFormattedDateString()
            : null;
    }

    /**
     * @return string
     */
    public function getTrialEndDateAttribute()
    {
        return !empty($this->trial_end_at)
            ? Carbon::parse($this->trial_end_at)->toFormattedDateString()
            : null;
    }

    /**
     * @param $period
     * @return bool
     */
    public function hasSalaryInPeriod($period)
    {
        return in_array($this->id, $period->salaryEmployeeIds);
    }
}