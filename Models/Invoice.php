<?php

namespace App\Models;

use Carbon\Carbon;
use App\Traits\CreatedByTrait;

class Invoice extends BaseModel
{
    use CreatedByTrait;

    const CURRENCY_USD = 1;
    const CURRENCY_EUR = 2;
    const CURRENCY_UAH = 3;

    const STATUS_REJECTED = -2;
    const STATUS_NOT_PAID = -1;
    const STATUS_NOT_SENT = 0;
    const STATUS_PAID = 1;

    /**
     * @var Setting
     */
    private $setting;

    /**
     * @var array
     */
    protected $fillable = [
        'client_id',
        'date_at',
        'time',
        'rate',
        'currency',
        'bonus',
        'discount',
        'gateway_id',
        'status',
        'from_date_at',
        'to_date_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'client_id' => 'integer',
        'date_at' => 'date',
        'time' => 'float',
        'rate' => 'float',
        'currency' => 'integer',
        'bonus' => 'float',
        'discount' => 'float',
        'gateway_id' => 'integer',
        'status' => 'integer',
        'created_by' => 'integer',
        'from_date_at' => 'from_date',
        'to_date_at' => 'to_date',
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'currency' => self::CURRENCY_USD,
        'status' => self::STATUS_NOT_SENT,
    ];

    /**
     * @var array
     */
    protected $order = [
        'created_at' => 'desc'
    ];

    /**
     * @var array
     */
    protected $search = [
        'id',
        'client_id',
    ];

    /**
     * @var string[]
     */
    protected $only = [
        'client_id',
        'id',
    ];

    /**
     * @var array
     */
    protected $with = [
        'client',
        'gateway',
    ];

    /**
     * @return array
     */
    public static function currencies()
    {
        return [
            self::CURRENCY_USD => __('USD'),
            self::CURRENCY_EUR => __('EUR'),
            self::CURRENCY_UAH => __('UAH'),
        ];
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_REJECTED => __('Rejected'),
            self::STATUS_NOT_SENT => __('Not Sent'),
            self::STATUS_NOT_PAID => __('Not Paid'),
            self::STATUS_PAID => __('Paid'),
        ];
    }

    /**
     * @return array
     */
    public static function statusBadgeClasses()
    {
        return [
            self::STATUS_REJECTED => 'warning',
            self::STATUS_NOT_PAID => 'danger',
            self::STATUS_NOT_SENT => 'pink',
            self::STATUS_PAID => 'success',
        ];
    }

    /**
     * Invoice constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->date_at = request('date_at') ?? Carbon::now();

        $this->setting = new Setting();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gateway()
    {
        return $this->belongsTo(PaymentGateway::class, 'gateway_id');
    }

    /**
     * @return string
     */
    public function getDateAttribute()
    {
        return !empty($this->date_at)
            ? Carbon::parse($this->date_at)->toFormattedDateString()
            : null;
    }

    /**
     * @return mixed
     */
    public function getCurrencyTextAttribute()
    {
        return self::currencies()[$this->currency] ?? null;
    }

    /**
     * @return mixed
     */
    public function getStatusTextAttribute()
    {
        return $this::statuses()[$this->status] ?? null;
    }

    /**
     * @return mixed
     */
    public function getStatusBadgeClassAttribute()
    {
        return self::statusBadgeClasses()[$this->status] ?? null;
    }

    /**
     * @return string
     */
    public function getFullIdAttribute()
    {
        return $this->setting->invoiceNamePrefix . '-' . $this->id;
    }

    /**
     * @return mixed|null
     */
    public function getAmountAttribute()
    {
        return $this->time * $this->rate;
    }

    /**
     * @return mixed|null
     */
    public function getTotalAttribute()
    {
        return $this->amount
            + $this->bonus
            - $this->amount * $this->discount / 100;
    }

    /**
     * @return string
     */
    public function getDueDateAttribute()
    {
        $daysAddedForDueDate = $this->setting->daysAddedForDueDate;

        return !empty($this->date_at)
            ? Carbon::parse($this->date_at)->addDays($daysAddedForDueDate)->toFormattedDateString()
            : null;
    }
}
