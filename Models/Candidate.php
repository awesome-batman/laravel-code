<?php

namespace App\Models;

use Carbon\Carbon;
use App\Traits\CreatedByTrait;

class Candidate extends BaseModel
{
    use CreatedByTrait;

    const STATUS_WE_REJECTED = -2;
    const STATUS_THEY_REJECTED = -1;
    const STATUS_NEW = 0;
    const STATUS_IN_PROCESS = 1;
    const STATUS_PAUSE = 2;
    const STATUS_RESERVE = 3;
    const STATUS_SUCCESS = 4;

    const MARK_BAD = 1;
    const MARK_BEARABLE = 2;
    const MARK_NORMAL = 3;
    const MARK_GOOD = 4;
    const MARK_GREAT = 5;
    const MARK_BEST_AMAZING = 6;

    const LEVEL_TRAINEE = 1;
    const LEVEL_JUNIOR = 2;
    const LEVEL_PRE_MIDDLE = 3;
    const LEVEL_MIDDLE = 4;
    const LEVEL_ADVANCED_MIDDLE = 5;
    const LEVEL_SENIOR = 6;

    const ENGLISH_BEGINNER = 1;
    const ENGLISH_ELEMENTARY = 2;
    const ENGLISH_PRE_INTERMEDIATE = 3;
    const ENGLISH_INTERMEDIATE = 4;
    const ENGLISH_UPPER_INTERMEDIATE = 5;
    const ENGLISH_ADVANCED = 6;
    const ENGLISH_MASTERY = 7;

    const TYPE_OFFICE = 1;
    const TYPE_REMOTE = 2;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'email',
        'position_id',
        'date_at',
        'interview_at',
        'cv_link',
        'test_link',
        'comment',
        'when_start',
        'info',
        'level',
        'experience_years',
        'experience_work',
        'skills',
        'education',
        'english_level',
        'english_comment',
        'type',
        'code',
        'mark',
        'bad_habits',
        'leave_reason',
        'status',
        'status_comment',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'position_id' => 'integer',
        'date_at' => 'date',
        'interview_at' => 'date',
        'level' => 'integer',
        'experience_years' => 'float',
        'english_level' => 'integer',
        'type' => 'integer',
        'mark' => 'integer',
        'status' => 'integer',
        'created_by' => 'integer',
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'status' => self::STATUS_NEW,
    ];

    /**
     * @var array
     */
    protected $order = [
        'created_at' => 'desc'
    ];

    /**
     * @var array
     */
    protected $search = [
        'id',
        'name',
        'phone',
        'email',
        'comment',
        'when_start',
        'info',
        'experience_work',
        'skills',
        'education',
        'english_comment',
        'bad_habits',
        'leave_reason',
        'status_comment',
    ];

    /**
     * SaleEvent constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->date_at = request('date_at') ?? Carbon::now();
    }

    /**
     * @return array
     */
    public static function marks()
    {
        return [
            self::MARK_BAD => __('Bad'),
            self::MARK_BEARABLE => __('Bearable'),
            self::MARK_NORMAL => __('Normal'),
            self::MARK_GOOD => __('Good'),
            self::MARK_GREAT => __('Great'),
            self::MARK_BEST_AMAZING => __('Best and Amazing'),
        ];
    }

    /**
     * @return array
     */
    public static function markBadgeClasses()
    {
        return [
            self::MARK_BAD => 'danger',
            self::MARK_BEARABLE => 'warning',
            self::MARK_NORMAL => 'secondary',
            self::MARK_GOOD => 'primary',
            self::MARK_GREAT => 'info',
            self::MARK_BEST_AMAZING => 'success',
        ];
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_NEW => __('New'),
            self::STATUS_IN_PROCESS => __('In Process'),
            self::STATUS_PAUSE => __('Pause'),
            self::STATUS_RESERVE => __('Reserve'),
            self::STATUS_WE_REJECTED => __('We Rejected'),
            self::STATUS_THEY_REJECTED => __('They Rejected'),
            self::STATUS_SUCCESS => __('Success'),
        ];
    }

    /**
     * @return array
     */
    public static function statusBadgeClasses()
    {
        return [
            self::STATUS_WE_REJECTED => 'danger',
            self::STATUS_THEY_REJECTED => 'warning',
            self::STATUS_NEW => 'info',
            self::STATUS_IN_PROCESS => 'success',
            self::STATUS_PAUSE => 'secondary',
            self::STATUS_RESERVE => 'primary',
            self::STATUS_SUCCESS => 'dark',
        ];
    }

    /**
     * @return array
     */
    public static function levels()
    {
        return [
            self::LEVEL_TRAINEE => __('Trainee'),
            self::LEVEL_JUNIOR => __('Junior'),
            self::LEVEL_PRE_MIDDLE => __('Pre-Middle'),
            self::LEVEL_MIDDLE => __('Middle'),
            self::LEVEL_ADVANCED_MIDDLE => __('Advanced-Middle'),
            self::LEVEL_SENIOR => __('Senior'),
        ];
    }

    /**
     * @return array
     */
    public static function levelBadgeClasses()
    {
        return [
            self::LEVEL_TRAINEE => 'danger',
            self::LEVEL_JUNIOR => 'warning',
            self::LEVEL_PRE_MIDDLE => 'info',
            self::LEVEL_MIDDLE => 'primary',
            self::LEVEL_ADVANCED_MIDDLE => 'primary',
            self::LEVEL_SENIOR => 'success',
        ];
    }

    /**
     * @return array
     */
    public static function englishLevels()
    {
        return [
            self::ENGLISH_BEGINNER => __('Beginner'),
            self::ENGLISH_ELEMENTARY => __('Elementary'),
            self::ENGLISH_PRE_INTERMEDIATE => __('Pre-Intermediate'),
            self::ENGLISH_INTERMEDIATE => __('Intermediate'),
            self::ENGLISH_UPPER_INTERMEDIATE => __('Upper-Intermediate'),
            self::ENGLISH_ADVANCED => __('Advanced'),
            self::ENGLISH_MASTERY => __('Mastery'),
        ];
    }

    /**
     * @return array
     */
    public static function englishLevelBadgeClasses()
    {
        return [
            self::ENGLISH_BEGINNER => 'danger',
            self::ENGLISH_ELEMENTARY => 'warning',
            self::ENGLISH_PRE_INTERMEDIATE => 'info',
            self::ENGLISH_INTERMEDIATE => 'primary',
            self::ENGLISH_UPPER_INTERMEDIATE => 'primary',
            self::ENGLISH_ADVANCED => 'success',
            self::ENGLISH_MASTERY => 'success',
        ];
    }

    /**
     * @return array
     */
    public static function types()
    {
        return [
            self::TYPE_OFFICE => __('Office Employee'),
            self::TYPE_REMOTE => __('Remote Employee'),
        ];
    }

    /**
     * @return array
     */
    public static function typeBadgeClasses()
    {
        return [
            self::TYPE_OFFICE => 'success',
            self::TYPE_REMOTE => 'warning',
        ];
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    /**
     * @return string
     */
    public function getDateAttribute()
    {
        return !empty($this->date_at)
            ? Carbon::parse($this->date_at)->toFormattedDateString()
            : null;
    }

    /**
     * @return string
     */
    public function getInterviewDateAttribute()
    {
        return !empty($this->interview_at)
            ? Carbon::parse($this->interview_at)->toFormattedDateString()
            : null;
    }

    /**
     * @return mixed
     */
    public function getMarkTextAttribute()
    {
        return self::marks()[$this->mark] ?? null;
    }

    /**
     * @return mixed
     */
    public function getMarkBadgeClassAttribute()
    {
        return self::markBadgeClasses()[$this->mark] ?? null;
    }

    /**
     * @return mixed
     */
    public function getStatusTextAttribute()
    {
        return self::statuses()[$this->status] ?? null;
    }

    /**
     * @return mixed
     */
    public function getStatusBadgeClassAttribute()
    {
        return self::statusBadgeClasses()[$this->status] ?? null;
    }

    /**
     * @return mixed
     */
    public function getLevelTextAttribute()
    {
        return self::levels()[$this->level] ?? null;
    }

    /**
     * @return mixed
     */
    public function getLevelBadgeClassAttribute()
    {
        return self::levelBadgeClasses()[$this->level] ?? null;
    }

    /**
     * @return mixed
     */
    public function getEnglishLevelTextAttribute()
    {
        return self::englishLevels()[$this->english_level] ?? null;
    }

    /**
     * @return mixed
     */
    public function getEnglishLevelBadgeClassAttribute()
    {
        return self::englishLevelBadgeClasses()[$this->english_level] ?? null;
    }

    /**
     * @return mixed
     */
    public function getTypeTextAttribute()
    {
        return self::types()[$this->type] ?? null;
    }

    /**
     * @return mixed
     */
    public function getTypeBadgeClassAttribute()
    {
        return self::typeBadgeClasses()[$this->type] ?? null;
    }
}