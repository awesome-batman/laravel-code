<?php
namespace App\Traits;

use App\Models\Period;
use Carbon\Carbon;

trait PeriodTrait
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function period()
    {
        return $this->belongsTo(Period::class, 'period_id');
    }

    /**
     * @return mixed
     */
    public function getPeriodTextAttribute()
    {
        return optional($this->period)->periodText;
    }

    /**
     * @return null
     */
    public function getPeriodDollarRateAttribute()
    {
        return optional($this->period)->dollar_rate;
    }

    /**
     * @return null
     */
    public function getPeriodWorkingDaysAttribute()
    {
        return optional($this->period)->working_days;
    }

    /**
     * @return null
     */
    public function getPeriodPensionTaxAttribute()
    {
        return optional($this->period)->pension_tax;
    }

    /**
     * @param $model
     */
    public static function associatePeriod($model)
    {
        $period = Period::recordByDate($model->date_at);

        if ($period->exists() && $period->id != $model->period_id) {
            $model->period()->associate($period);
        }
    }

    /**
     * return null
     */
    public function initDateAt()
    {
        $this->date_at = Carbon::now();
    }

    /**
     * @return bool
     */
    public function isCorrectPeriod()
    {
        $date = Carbon::parse($this->date_at);
        $period = $this->period;

        if (empty($period)) {
            return false;
        }

        return $date->year == $period->year
            && $date->month == $period->month;
    }
}