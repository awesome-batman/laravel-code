<?php
namespace App\Traits;

trait SalaryTrait
{
    /**
     * @return array
     */
    public static function currencies()
    {
        return [
            self::CURRENCY_UAH => __('UAH'),
            self::CURRENCY_USD => __('USD'),
        ];
    }

    /**
     * @return array
     */
    public static function rateTypes()
    {
        return [
            self::RATE_TYPE_MONTHLY => __('Monthly'),
            self::RATE_TYPE_HOURLY => __('Hourly'),
        ];
    }

    /**
     * @return array
     */
    public static function rateTypeCaptions()
    {
        return [
            self::RATE_TYPE_MONTHLY => __('per month'),
            self::RATE_TYPE_HOURLY => __('per hour'),
        ];
    }

    /**
     * @return array
     */
    public static function rateTypeBadgeClasses()
    {
        return [
            self::RATE_TYPE_MONTHLY => 'info',
            self::RATE_TYPE_HOURLY => 'success',
        ];
    }

    /**
     * @return array
     */
    public static function gateways()
    {
        return [
            self::GATEWAY_PAYONEER => __('Payoneer'),
            self::GATEWAY_BANK => __('Bank'),
            self::GATEWAY_CACHE => __('Cache'),
        ];
    }

    /**
     * @return array
     */
    public static function gatewayBadgeClasses()
    {
        return [
            self::GATEWAY_PAYONEER => 'success',
            self::GATEWAY_BANK => 'info',
            self::GATEWAY_CACHE => 'warning',
        ];
    }

    /**
     * @return array
     */
    public static function entrepreneurStatuses()
    {
        return [
            self::ENTREPRENEUR_NO => __('No'),
            self::ENTREPRENEUR_PENSION_COVERAGE => __('Pension Coverage'),
            self::ENTREPRENEUR_FULL_COVERAGE => __('Full Coverage'),
        ];
    }

    /**
     * @return array
     */
    public static function entrepreneurBadgeClasses()
    {
        return [
            self::ENTREPRENEUR_NO => 'secondary',
            self::ENTREPRENEUR_PENSION_COVERAGE => 'success',
            self::ENTREPRENEUR_FULL_COVERAGE => 'info',
        ];
    }

    /**
     * @return mixed
     */
    public function getRateTypeTextAttribute()
    {
        return $this::rateTypes()[$this->rate_type] ?? null;
    }

    /**
     * @return mixed
     */
    public function getRateTypeCaptionAttribute()
    {
        return $this::rateTypeCaptions()[$this->rate_type] ?? null;
    }

    /**
     * @return mixed
     */
    public function getRateTypeBadgeClassAttribute()
    {
        return self::rateTypeBadgeClasses()[$this->rate_type] ?? null;
    }

    /**
     * @return mixed
     */
    public function getGatewayTextAttribute()
    {
        return $this::gateways()[$this->gateway] ?? null;
    }

    /**
     * @return mixed
     */
    public function getGatewayBadgeClassAttribute()
    {
        return self::gatewayBadgeClasses()[$this->gateway] ?? null;
    }

    /**
     * @return mixed
     */
    public function getEntrepreneurTextAttribute()
    {
        return $this::entrepreneurStatuses()[$this->entrepreneur] ?? null;
    }

    /**
     * @return mixed
     */
    public function getEntrepreneurBadgeClassAttribute()
    {
        return self::entrepreneurBadgeClasses()[$this->entrepreneur] ?? null;
    }

    /**
     * @return int
     */
    public function getTotalAttribute()
    {
        return $this->salaryPayment + $this->payonerPayment;
    }

    /**
     * @return float|int
     */
    public function getOppositeTotalAttribute()
    {
        return $this->isUah
            ? $this->usdTotal
            : $this->uahTotal;
    }

    /**
     * @return \App\Traits\string\|float|int|null
     */
    public function getUahTotalAttribute()
    {
        return $this->isUah
            ? $this->total
            : $this->usdToUah($this->total);
    }

    /**
     * @return \App\Traits\string\|float|int|null
     */
    public function getUsdTotalAttribute()
    {
        return $this->isUsd
            ? $this->total
            : $this->uahToUsd($this->total);
    }

    /**
     * @return float|int
     */
    public function getSalaryWithoutTaxAttribute()
    {
        return (float) ($this->salary
            + $this->overtimePayment
            + $this->bonus);
    }

    /**
     * @return float|int
     */
    public function getSalaryPaymentAttribute()
    {
        return (float) ($this->salaryWithoutTax
            + $this->entrepreneurPayment);
    }

    /**
     * @return float|int
     */
    public function getOvertimePaymentAttribute()
    {
        $workingHoursPerDay = $this->setting->workingHoursPerDay;
        $overtimeCoefficient = $this->setting->overtimeCoefficient;

        return $this->salary
            / ($this->periodWorkingDays * $workingHoursPerDay)
            * $this->overtime
            * ($this->overtime > 0 ? $overtimeCoefficient : 1);
    }

    /**
     * @return float|int
     */
    public function getEntrepreneurPaymentAttribute()
    {
        return (float) ($this->pensionTaxPayment + $this->unifiedTaxPayment);
    }

    /**
     * @return float|int
     */
    public function getPensionTaxPaymentAttribute()
    {
        if ($this->entrepreneur == self::ENTREPRENEUR_NO) {
            return 0;
        }

        return $this->isUah
            ? $this->periodPensionTax
            : $this->uahToUsd($this->periodPensionTax);
    }

    /**
     * @return float|int
     */
    public function getUnifiedTaxPaymentAttribute()
    {
        if ($this->entrepreneur != self::ENTREPRENEUR_FULL_COVERAGE) {
            return 0;
        }

        $payment = $this->salaryWithoutTax + $this->pensionTaxPayment;
        $unifiedTaxPercent = $this->setting->unifiedTaxPercent;
        $unifiedCoefficient = (100 - $unifiedTaxPercent) / 100;

        return $payment / $unifiedCoefficient - $payment;
    }

    /**
     * @return \App\Traits\string\|float|int|null
     */
    public function getUnifiedTaxUahPaymentAttribute()
    {
        return $this->isUah
            ? $this->unifiedTaxPayment
            : $this->usdToUah($this->unifiedTaxPayment);
    }

    /**
     * @return float|int
     */
    public function getPayonerPaymentAttribute()
    {
        $payoneerPercent = $this->setting->payoneerPercent;

        return $this->gateway == self::GATEWAY_PAYONEER
            ? $this->salaryPayment * 100 / (100 - $payoneerPercent) - $this->salaryPayment
            : 0;
    }
}